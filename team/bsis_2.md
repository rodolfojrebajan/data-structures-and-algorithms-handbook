# Aspiring Data Analyst

## Alicia Jane Medina

👋 Aspiring Data Analyst!🤵✈️⚽— 💌 aliciajanemedina@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_medina_aliciajane.jpg](images/bsis_2_medina_aliciajane.jpg)

### Bio

**Good to know:** I like drawing. Turning imagination into reality is a spectacular thing! I find information handling, and creating decisions from critical thinking, fun and thrilling. Collecting information and being organized is efficient in doing tasks and keeps me from being insane :)).

**Motto:** Failure is not an option, try until the very end.

**Languages:** Java, SQL

**Other Technologies:** MS Office, ibisPaintX, SketchUp

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/b690e0faa45ee)

<!-- END -->

# Aspiring Game Designer

## Allen Murphy Bactol

👋 Aspiring Game Designer 🎮🕹 — 💌 allenmurphybactol@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_bactol_allenmurphy3.jpg](images/bsis_2_bactol_allenmurphy.jpg)

### Bio

**Good to know:** ...that, I'm alive.

**Motto:** ['ASMR: Art Speaks More Resonance.', 'Inspiration begets the motivation for innovation.']

**Languages:** Python, Java, SQL

**Other Technologies:** GameMaker, Google Workspace, Medibang

**Personality Type:** [Turbulent Logician (INTP-T)](https://www.16personalities.com/profiles/d471cce65a8c6)

<!-- END -->

# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Web Developer

## Rizalyne Asaldo

👋 Aspiring Web Developer! 👨‍💻 — 💌 rizalyneasaldo@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_asaldo_rizalyne.jpg](images/bsis_2_asaldo_rizalyne.jpg)

### Bio

**Good to know:** i'm a potato lover.

**Motto:** atleast i try.

**Languages:** Java, C#, HTML, Filipino and English.

**Other Technologies:** cellphone saka laptop.

**Personality Type:** [Logistician (ISTJ-T)](https://www.16personalities.com/istj-personality)

<!-- END -->

# Aspiring CEO

## Jerome Imperial

👋 Future CEO!🤵✈️⚽— 💌 jeromeimperial@laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_jerome_imperial.jpg](images/bsis_2_jerome_imperial.jpg)

### Bio

**Good to know:** I have little fear when aiming for towering goals. I might fail, but with God's help, my 'failure' (if you can call it that) will be better than most successes. Nonetheless, I pray for everyone's wellness 😉.

**Motto:** Obsession beats talent🏆🐢..................🐇

**Languages:** Python, Java, C++, SQL

**Other Technologies:** MS Excel, Anaconda's Jupyter Notebook, Arduino UNO

**Personality Type:** [Architect (INTJ-T)](https://www.16personalities.com/profiles/249d8eea60d58) from [Advocate (INFJ-T) ](https://www.16personalities.com/profiles/0d91b1539f64a) 3 years ago.

<!-- END -->

# Aspiring Software Developer

## Christian Eliseo Isip

👋 Aspiring Software Developer! ⌨️🖥️🖱️ — 💌 christianeliseoisip@laverdad.edu.ph — Apalit, Pampanga

![bsis_2_isip_christian.jpg](images/bsis_2_isip_christian.jpg)

### Bio

**Good to know:** I tend to complain a lot whenever things get tough, but I always find a way to get the job done!🫡 To my fellow complainers, turn our complaints into accomplishments🏆✨, one gripe at a time!🙌

**Motto:** Complain first, conquer next!🦾

**Languages:** Java, C++, SQL

**Other Technologies:** MS Word, MS PPT, Canva, VS code, Iphone 14 plus🤓

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/c1a9580537bf1)

<!-- END -->

# Aspiring Graduate

## Daniel Latina

👋 Aspiring Graduate! 🚀👨‍💻 — 💌 daniellatina@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_latina_daniel.jpg](images/bsis2_latina_daniel.jpg)

### Bio

**Good to know:** I'm  Bald. One thing for sure I've learned throughout my life is that I'm gettng bald earlier than I anticipated and it's is the most significant part of my life (getting bald). I prefer having hair than being bald.

**Motto:** Preserve hair while they're there

**Languages:** Java, Python, HTML, CSS

**Other Technologies:** VS Code, MySQL

**Personality Type:** [(ISFP-T)](https://www.16personalities.com/infp-personality)

# Aspiring Yaya

## Justine Jynne Patrice A. Marco

🙂 Aspiring Yaya ng mga anak niyo! 🤡❤️‍🔥 —  💬 justinejynnepatricemarco@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_marco_justine.jpg](images/bsis_2_marco_justine.jpg)

### Bio

**Good to know:** I go by many names but hyper is one of them chz na hindi. It's just the way I show my enthusiasm. You may call me Justine or Marco. My favorite color is blue because it's the color of the plants po, mahilig po kasi si Lola sa plants kaya po parati po kami nagbi-beach gusto po niya makita yung ocean.

**Motto:** I will not water myself down to make me more digestible for you. You can choke.

**Languages:** English, Tagalog, Tambay sa Kanto

**Other Technologies:** Pinterest, Instagram

**Personality Type:** [Advocate (INFJ-T)](https://www.16personalities.com/profiles/a94cda034d40b)

<!-- END -->

# Aspiring Degree Holder

## Elloisa Degula

👋 Aspiring Degree Holder! 👨‍💻 — 💌 elloisadegula@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_degula_elloisa.jpg](images/bsis_2_degula_elloisa.jpg)

### Bio

**Good to know:** I love moon and cats a lot. I am quiet and super loud at the same time but it depends to the situation. I'm into sports.

**Motto:** Time is Gold because you cannot bring back time.

**Languages:** Java, C#, mysql

**Other Technologies:** Visual Studio

**Personality Type:** [Campaigner (ENFP-T)](https://www.16personalities.com/profiles/0487b57c646f7)

<!-- END -->

# Aspiring Hacker

## Sherelyn Cuanan

👋 Aspiring Hacker! 🚀👨‍💻 — 💌 sherelyncuanan@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_cuanan_sherelyn.jpg](images/bsis_2_cuanan_sherelyn.jpg)

### Bio

**Good to know:** I love listening to music, especially OPM, R&B, old songs, classics, etc. I do play some basic ukulele instruments. I love learning new things, even if I am a slow learner. I prefer to be silent because it is better than to betray myself and talk unwisely.

**Motto:** Sometimes you win sometimes you learn.

**Languages:** JAVA, SQL, English,hatdog

**Other Technologies:** VS Code

**Personality Type:** [Mediator (INFP-T)](https://www.16personalities.com/profiles/1e47476a72815)

<!-- END -->

# Aspiring APK Programmer 

## Jean Rose Talen 

🤷‍♀️Aspiring APK Programmer! 🌃👩‍💻— jeanrosetalen@laverdad.edu.ph — Apalit, Pampanga 

![alt bsis_2_talen_jeanrose.jpg](images/bsis_2_talen_jeanrose.jpg) 

### Bio 

**Good to know:** I am somewhat interested in logic and mathematics *BEFORE* that I kinda enjoy programming. All of those disappeared when I entered college. *jk*

**Motto:** With great imagination comes great results. *eme*

**Languages:** Java, HTML, MySQL 

**Other Technologies:** Google Workspace, Microsoft 

**Personality Type:** [Adventurer (ISFP-T)](https://www.16personalities.com/isfp-personality) 

# Aspiring Rich Tita

## Trisha Olorvida

👋 Aspiring Rich Tita!  — 💌 trishamayolorvida@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_olorvida_trisha.jpg](images/bsis_2_olorvida_trisha.jpg)

### Bio

**Good to know:** I'm just a silly human with silly thoughts. I like playing games with friends. I love the color pink and anything cute. I play Genshin Impact (dm for uid). I like listening to music, my current favorite artists/bands are deftones, ptv, mitski, laufey, and zild (medyo emo si ate mo)

**Motto:** Money IS Happiness

**Languages:** HTML, Javascript, Korean, English, Japanese

**Other Technologies:** VSCode, AutoCAD, Adobe, Microsoft

**Personality Type:** [Logician (INTP-T)](https://www.16personalities.com/intp-personality)

<!-- END -->

# Gcash Mod: Unlimited Money APK Developer

## Princess Olingay

💸 Gcash Mod: Unlimited Money APK Developer! 🤑💰 — 💌 princessolingay@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_olingay_princess.jpg](images/bsis_2_olingay_princess.jpg)

### Bio

**Good to know:** Money is like the 6th sense that allows you to enjoy the other 5. *(Satirical)*

**Motto:** When it comes to anything in life-- relationships, friendships, the work you do, the art you make, the music you hear -- when it comes to ***anything***, if it doesn't create an avalanche within your chest, if it doesn't move you and inspire you, if it does not come from the deepest part of who you are, ***it is not for you***.

**Languages:** Basic Java, HTML, MySQL, Money

**Other Technologies:** Microsoft Suite, Google Workspace

**Personality Type:** [Protagonist (ENFJ-T)](https://www.16personalities.com/enfj-personality)

<!-- END -->

# Ambitious IT Professional

## John Miguel Mañabo

:love_you_gesture::peach: Ambitious IT Professional <br> 
:email: johnmiguelmanabo@student.laverdad.edu.ph <br>
:house: Apalit, Pampanga

![bsis_2_manabo_miguel.jpg](images/bsis_2_manabo_miguel.jpg)

## Bio

**Good to know:** *That I am still in the track*

**Motto:** *Long before armament and medicine were invented, there are such words that have powers to save a soul*

**Languages:** *Java, C#, HTML, CSS, JavaScript, Love*

**Other Technologies:** *Unity studios, blender, eclipse, etc.*

**Personality Type:** [Turbulent Consul (ESFJ-T)](https://www.16personalities.com/esfj-personality)

<!-- END -->

# Aspiring Web Developer

## Louie David Tubat

👋 Aspiring Web Developer! 🚀👨‍💻 — 💌 louiedavidtubat@laverdad.edu.ph — Apalit, Pampanga

![bsis_2_tubat_louiedavid.jpg](images/bsis_2_tubat_louiedavid.jpg)

## Bio

**Good to know:** I am a simple person who works quietly and fights in the middle of many trials to achieve the desired dream.

**Motto:** as it weakens it gets stronger.


**Languages:** Python, Java, CSS, V-WarayWaray, V-Cebuano

**Other Technologies:** VS Code, replit, Udemy

**Personality Type:** [Turbulent Consul (ESFJ-T)](https://www.16personalities.com/esfj-personality)

# Aspiring Jumbo Hatdog

## Ma.Hermosa C. Malapit

👋 Aspiring Jumbo Hatdog! 🚀👨‍💻 — 💌 mahermosamalapit@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_malapit_ma.hermosa.jpg](<images/bsis_2_malapit_ma.hermosa.jpg)

### Bio

**Good to know:**  Creativity is one of my skills. I admire arts, and I can be more artistic. I'm just too lazy to create more new art works.huyy.. kimii 
hindi tayo magiging assumera for today's videyow!

**Motto:** Art is not a thing, It is a way.

**Languages:** SQL,JAVA, English carabao, Taglish

**Other Technologies:** VS Code, Canva, Adobe

**Personality Type:** [Mediator (INFP-T)](https://www.16personalities.com/profiles/c9cd48a6ae98d)

# Aspiring Web Developer

## Jasmine T. Manansala

👋 Aspiring Web Developer! 🚀👨‍💻 — 💌 jasminemanansala@student.laverdad.edu.ph — Macabebe, Pampanga

![alt bsis_2_manansala_jasmine.jpg](<images/bsis_2_manansala_jasmine.jpg>)

### Bio

**Good to know:** An introverted friendly, loud, talkative, and direct person who sees fairness in all things as much as possible that I can. I also have the initiative to do things as long as I am fine with the task given. A Virgo, loves nature and music, and also someone who loves Anime and Japan culture. I like doing deep and quality talks with others instead of shallow conversations as I am direct and genuine with my thoughts and feelings. You can say I am a serious yet a happy hooman being living in planet Earth.

**Motto:** Experiences and their realization makes us.

**Languages:** C++, Java, SQL, English, Tagalog, Kapampangan, Japanese


**Other Technologies:** VS code, MS Word, Photoshop, Adobe Premiere Pro

**Personality Type:** [Adventurer (ISFP-T)](https://www.16personalities.com/profiles/a0edcdffe24fc)


<!-- END -->
